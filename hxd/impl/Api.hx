package hxd.impl;

class Api {

	public static inline function downcast<T:{},S:T>( value : T, c : Class<S> ) : S {
		return Std.instance(value,c);
	}

}